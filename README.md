# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Test Assignment
* Sorting & Showing data from api in fragment with offline feature
* Version -1

### Tools, Plugins and Libraries used###

* MVVM, Kotlin, Dagger 2, Retrofit2, Room, Repository System, Unit Testing, Jacoco
* Instrumentation Testing, Mockito, Robolectric, JUnit, Livedata, Kotlin Coroutines

### How do I get set up? ###

* Clone the repository and move to master branch
* Run application it will work
* Run test cases through test folder
* Run tests with coverage 

### Things to do in future ###

* Write UI test cases
* Achieve 100% test coverage