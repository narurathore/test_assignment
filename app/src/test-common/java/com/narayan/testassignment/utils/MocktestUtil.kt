package com.narayan.testassignment.utils

import com.narayan.testassignment.data.models.Album

class MocktestUtil {

    companion object {

        fun mockAlbum(id: Int) = Album(1, id, "mock-title")

        fun mockAlbum(id: Int, title: String) = Album(1, id, title)

        fun mockAlbumList(): ArrayList<Album> {
            val movies = ArrayList<Album>()
            movies.add(mockAlbum(1))
            movies.add(mockAlbum(2))
            movies.add(mockAlbum(3))
            return movies
        }
    }
}