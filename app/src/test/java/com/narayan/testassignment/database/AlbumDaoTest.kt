package com.narayan.testassignment.database

import com.narayan.testassignment.data.models.Album
import com.narayan.testassignment.utils.MocktestUtil
import kotlinx.coroutines.*
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [23])
class AlbumDaoTest: LocalDatabase() {

    @Test
    fun insertAndReadTest() {
        runBlocking {
            testSimpleInsertAndRead()
            testInsertAndReadOfDuplicateItem()
        }
    }

    private suspend fun testSimpleInsertAndRead(){
        val mockAlbums = MocktestUtil.mockAlbumList()
        albumsDatabase.albumDao().deleteAlbums()
        albumsDatabase.albumDao().insertAlbums(mockAlbums)
        val albumsFromDB = albumsDatabase.albumDao().getAllAlbums()
        Assert.assertEquals("simple albums test", mockAlbums, albumsFromDB)
    }

    private suspend fun testInsertAndReadOfDuplicateItem(){
        val mockAlbums = ArrayList<Album>()
        mockAlbums.add(MocktestUtil.mockAlbum(1))
        albumsDatabase.albumDao().deleteAlbums()
        albumsDatabase.albumDao().insertAlbums(mockAlbums)
        albumsDatabase.albumDao().insertAlbums(mockAlbums)
        val albumsFromDB = albumsDatabase.albumDao().getAllAlbums()
        Assert.assertEquals("duplicate album test", mockAlbums, albumsFromDB)
    }
}