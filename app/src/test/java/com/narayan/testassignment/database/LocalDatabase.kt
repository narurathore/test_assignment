package com.narayan.testassignment.database

import androidx.room.Room
import org.junit.Before
import org.robolectric.annotation.Config
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.narayan.testassignment.data.database.AlbumDatabase
import org.junit.After
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [23])
abstract class LocalDatabase {
    lateinit var albumsDatabase: AlbumDatabase

    @Before
    fun initDB(){
        albumsDatabase = Room.inMemoryDatabaseBuilder(getApplicationContext(), AlbumDatabase::class.java)
            .allowMainThreadQueries().build()
    }

    @After
    fun closeDB() {
        albumsDatabase.close()
    }
}