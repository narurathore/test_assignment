package com.narayan.testassignment.util

import com.narayan.testassignment.data.models.Album
import com.narayan.testassignment.utilities.sortByTitle
import com.narayan.testassignment.utils.MocktestUtil
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class UtilTest {

    @Test
    fun sortAlbumDataTest(){

        // All values are same
        val albums = ArrayList<Album>()
        albums.add(MocktestUtil.mockAlbum(1, "a"))
        albums.add(MocktestUtil.mockAlbum(2, "a"))
        albums.add(MocktestUtil.mockAlbum(3, "a"))
        var sortedAlbums = albums.sortByTitle()
        Assert.assertEquals(1,sortedAlbums[0].id)
        Assert.assertEquals(2,sortedAlbums[1].id)
        Assert.assertEquals(3,sortedAlbums[2].id)

        // already sorted
        albums.clear()
        albums.add(MocktestUtil.mockAlbum(1, "a"))
        albums.add(MocktestUtil.mockAlbum(2, "b"))
        albums.add(MocktestUtil.mockAlbum(3, "c"))
        sortedAlbums = albums.sortByTitle()
        Assert.assertEquals(1,sortedAlbums[0].id)
        Assert.assertEquals(2,sortedAlbums[1].id)
        Assert.assertEquals(3,sortedAlbums[2].id)

        // unsorted
        albums.clear()
        albums.add(MocktestUtil.mockAlbum(3, "bdfdf"))
        albums.add(MocktestUtil.mockAlbum(2, "aJgk"))
        albums.add(MocktestUtil.mockAlbum(1, "aBkjk"))
        sortedAlbums = albums.sortByTitle()
        Assert.assertEquals(1,sortedAlbums[0].id)
        Assert.assertEquals(2,sortedAlbums[1].id)
        Assert.assertEquals(3,sortedAlbums[2].id)
    }
}