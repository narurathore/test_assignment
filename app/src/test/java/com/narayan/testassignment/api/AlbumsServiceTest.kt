package com.narayan.testassignment.api

import com.narayan.testassignment.data.api.AlbumsServiceInterface
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class AlbumsServiceTest : ApiAbstract<AlbumsServiceInterface>() {

    private lateinit var service: AlbumsServiceInterface

    @Before
    fun initService() {
        this.service = createService(AlbumsServiceInterface::class.java)
    }

    @Test
    fun fetchAlbumsTest() {
        enqueueResponse("/albums.json")
        runBlocking {
            val response = service.getDataFromAPI()
            Assert.assertEquals(5, response.size)
            Assert.assertEquals(1,response[0].id)
            Assert.assertEquals("quidem molestiae enim",response[0].title)
        }
    }
}