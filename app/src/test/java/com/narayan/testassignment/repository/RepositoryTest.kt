package com.narayan.testassignment.repository

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.narayan.testassignment.data.api.AlbumsServiceInterface
import com.narayan.testassignment.data.database.AlbumsDao
import com.narayan.testassignment.data.repository.Repository
import com.narayan.testassignment.utils.MocktestUtil
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(JUnit4::class)
class RepositoryTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var repository: Repository
    @MockK private lateinit var albumDao: AlbumsDao
    @MockK private lateinit var service: AlbumsServiceInterface

    @Before
    fun init() {
        MockKAnnotations.init(this)
        repository = Repository(service, albumDao)
    }

    @Test
    fun loadAlbumsFromNetwork() {
        val mockAlbums = MocktestUtil.mockAlbumList()
        runBlocking {
            coEvery {albumDao.getAllAlbums()} returns ArrayList()
            coEvery {service.getDataFromAPI()} returns mockAlbums
            coEvery {albumDao.deleteAlbums()} returns Unit
            coEvery {albumDao.insertAlbums(any())} returns Unit
            var data = repository.getAlbumsData()
            Assert.assertEquals(mockAlbums, data)

            val mockDBAlbums = MocktestUtil.mockAlbumList()
            mockDBAlbums.add(MocktestUtil.mockAlbum(789))
            coEvery {albumDao.getAllAlbums()} returns mockDBAlbums
            coEvery {service.getDataFromAPI()} returns mockAlbums
            data = repository.getAlbumsData()
            Assert.assertEquals(mockAlbums, data)
        }
    }

    @Test
    fun loadAlbumsFromDBWithWithExceptionAndDataFromDao() {
        val mockAlbums = MocktestUtil.mockAlbumList()
        runBlocking {
            coEvery {albumDao.getAllAlbums()} returns mockAlbums
            coEvery {service.getDataFromAPI()} throws Exception()
            val data = repository.getAlbumsData()
            Assert.assertEquals(mockAlbums, data)
        }
    }

    @Test
    fun loadAlbumsFromDBWithException() {
        val mockAlbums = MocktestUtil.mockAlbumList()
        runBlocking {
            coEvery {albumDao.getAllAlbums()} returns mockAlbums
            coEvery {service.getDataFromAPI()} throws Exception()
            try {
                repository.getAlbumsData()
            }catch (ex: Exception){
                Assert.assertTrue(true)
            }

        }
    }
}