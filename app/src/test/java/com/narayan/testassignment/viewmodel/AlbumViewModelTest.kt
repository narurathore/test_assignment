package com.narayan.testassignment.viewmodel

import com.narayan.testassignment.data.api.AlbumsServiceInterface
import com.narayan.testassignment.data.database.AlbumsDao
import com.narayan.testassignment.data.repository.Repository
import com.narayan.testassignment.utils.MocktestUtil
import com.narayan.testassignment.ui.albums.viewmodels.AlbumViewModel
import com.narayan.testassignment.utilities.Resource
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.*
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class AlbumViewModelTest {

    private lateinit var viewModel: AlbumViewModel

    @MockK private lateinit var albumsDao: AlbumsDao
    @MockK private lateinit var service: AlbumsServiceInterface

    @Before
    fun init() {
        MockKAnnotations.init(this)
        viewModel = AlbumViewModel(Repository(service, albumsDao))
    }

    @Test
    fun testMakeApiCallSuccessBothDataSuccess() {

        CoroutineScope(Dispatchers.Main).launch {
            val mockAlbums = MocktestUtil.mockAlbumList()
            coEvery {service.getDataFromAPI()} returns mockAlbums
            coEvery {albumsDao.getAllAlbums()} returns mockAlbums
            viewModel.resource.observeForever {
                Assert.assertEquals(it, Resource.success(mockAlbums))
            }
            viewModel.fetchData()
            delay(1000)
        }
    }

    @Test
    fun testMakeApiCallSuccessDatabaseSuccessApiFail() {

        CoroutineScope(Dispatchers.Main).launch {
            val mockAlbums = MocktestUtil.mockAlbumList()
            coEvery {service.getDataFromAPI()} throws Exception()
            coEvery {albumsDao.getAllAlbums()} returns mockAlbums
            viewModel.resource.observeForever {
                Assert.assertEquals(it, Resource.success(mockAlbums))
            }
            viewModel.fetchData()
            delay(1000)
        }

    }

    @Test
    fun testMakeApiCallException() {
       CoroutineScope(Dispatchers.Main).launch {
           coEvery {service.getDataFromAPI()} throws Exception()
           coEvery {albumsDao.getAllAlbums()} returns ArrayList()
           viewModel.resource.observeForever {
               Assert.assertEquals(it, Resource.error( null, null))
           }
           viewModel.fetchData()
           delay(1000)
       }
    }
}