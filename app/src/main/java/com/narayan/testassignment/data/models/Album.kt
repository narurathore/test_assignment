package com.narayan.testassignment.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Album data class
 */
@Entity(tableName = "albums")
data class Album(

    val userId: Int,

    @PrimaryKey val id: Int,

    val title: String
)
