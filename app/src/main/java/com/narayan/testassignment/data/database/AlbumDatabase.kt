package com.narayan.testassignment.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.narayan.testassignment.data.models.Album

/**
 * Database to store albums
 */
@Database(entities = [Album::class], version = 1, exportSchema = false)
abstract class AlbumDatabase: RoomDatabase() {

    /**
     * Will return Dao for Album
     */
    abstract fun albumDao(): AlbumsDao
}