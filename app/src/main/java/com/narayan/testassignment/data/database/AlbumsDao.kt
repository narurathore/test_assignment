package com.narayan.testassignment.data.database

import androidx.room.*
import com.narayan.testassignment.data.models.Album

/**
 * Album Dao to execute query
 */
@Dao
interface AlbumsDao {

    /**
     * Will insert list of albums in table
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAlbums(albums: List<Album>)

    /**
     * Will empty the albums table
     */
    @Query("DELETE from albums")
    suspend fun deleteAlbums()

    /**
     * Will return the all data from albums table
     */
    @Query("SELECT * from albums")
    suspend fun getAllAlbums(): List<Album>
}