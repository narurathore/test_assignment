package com.narayan.testassignment.data.repository

import com.narayan.testassignment.data.api.AlbumsServiceInterface
import com.narayan.testassignment.data.database.AlbumsDao
import com.narayan.testassignment.data.models.Album
import com.narayan.testassignment.utilities.sortByTitle
import javax.inject.Inject

/**
 * Repository for albums
 */
class Repository @Inject constructor(private val retroService: AlbumsServiceInterface
    , val albumsDao: AlbumsDao) {

    /**
     * Will return data on successful response, will return data from DB if no network
     * will throw exception for any other exception
     */
    @Throws(Exception::class)
    suspend fun getAlbumsData(): List<Album> {
        try {
            val albums = retroService.getDataFromAPI()
            albumsDao.deleteAlbums()
            albumsDao.insertAlbums(albums)
            return albums.sortByTitle()
        }catch (ex: Exception){
            val albums = albumsDao.getAllAlbums()
            if (albums.isNotEmpty()){
                return albums.sortByTitle()
            }
            throw ex
        }
    }

}