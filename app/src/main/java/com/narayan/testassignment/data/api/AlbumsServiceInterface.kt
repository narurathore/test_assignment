package com.narayan.testassignment.data.api

import com.narayan.testassignment.data.models.Album
import retrofit2.http.GET

/**
 * Album api interface to get data from api
 */
interface AlbumsServiceInterface {

    /**
     * Will return fetched list of albums from server
     */
    @GET("albums")
    suspend fun getDataFromAPI(): ArrayList<Album>
}