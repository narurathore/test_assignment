package com.narayan.testassignment.ui.albums.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.narayan.testassignment.data.repository.Repository
import com.narayan.testassignment.data.models.Album
import com.narayan.testassignment.utilities.Resource
import com.narayan.testassignment.utilities.Status
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * View model which handled albums
 */
@HiltViewModel
class AlbumViewModel @Inject constructor(private val repo: Repository) : ViewModel() {

    val resource: MutableLiveData<Resource<List<Album>>> = MutableLiveData()

    /**
     * Will get data from repository and update live data variables accordingly
     */
    fun fetchData(){
        viewModelScope.launch(Dispatchers.IO) {
            try {
                resource.postValue(Resource.loading(null))
                resource.postValue(Resource(Status.SUCCESS, repo.getAlbumsData(), null))
            }catch (ex: Exception){
                resource.postValue(Resource.error(null, null))
            }
        }
    }

}