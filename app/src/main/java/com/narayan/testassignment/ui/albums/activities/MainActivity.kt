package com.narayan.testassignment.ui.albums.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.narayan.testassignment.databinding.ActivityMainBinding
import com.narayan.testassignment.ui.albums.fragments.AlbumListFragment
import dagger.hilt.android.AndroidEntryPoint

/**
 * Main launcher activity of application. Loads Album list fragment
 */
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Init binding object and set content view
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        //Set Toolbar for activity
        setSupportActionBar(binding?.toolbar)

        //Set up recycler view fragment
        setUpFragment()
    }

    /**
     * Will setup Album List Fragment and launch it in container view
     */
    private fun setUpFragment(){
        binding?.container?.id?.let {
            supportFragmentManager.beginTransaction()
                .replace(it,AlbumListFragment.newInstance())
                .commitAllowingStateLoss()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

}