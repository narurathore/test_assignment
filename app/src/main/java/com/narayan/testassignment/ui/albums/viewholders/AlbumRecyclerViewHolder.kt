package com.narayan.testassignment.ui.albums.viewholders

import androidx.recyclerview.widget.RecyclerView
import com.narayan.testassignment.databinding.RecyclerViewItemAlbumBinding
import com.narayan.testassignment.data.models.Album

/**
 * View holder to show album data
 */
class AlbumRecyclerViewHolder(private val dataBinding: RecyclerViewItemAlbumBinding):
    RecyclerView.ViewHolder(dataBinding.root) {

    /**
     * will bind the album data to view
     */
    fun onBind(album: Album){
        dataBinding.album = album
    }
}