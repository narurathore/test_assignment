package com.narayan.testassignment.ui.albums.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.narayan.testassignment.ui.albums.adapters.AlbumRecyclerViewAdapter
import com.narayan.testassignment.databinding.FragmentAlbumListBinding
import com.narayan.testassignment.data.models.Album
import com.narayan.testassignment.ui.albums.viewmodels.AlbumViewModel
import com.narayan.testassignment.utilities.*
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 * Use the [AlbumListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class AlbumListFragment : Fragment(){

    private var binding: FragmentAlbumListBinding? = null
    private var albumRecyclerViewAdapter: AlbumRecyclerViewAdapter? = null
    private val viewModel: AlbumViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentAlbumListBinding.inflate(layoutInflater)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initViewModel()
        initClickListener()
    }

    /**
     * Will init all click listeners used in this fragment
     */
    private fun initClickListener(){
        binding?.retryButton?.setOnClickListener {
            viewModel.fetchData()
        }
    }

    /**
     * Will init recycler view
     */
    private fun initRecyclerView() {
        albumRecyclerViewAdapter = with(binding?.recyclerView) {
            this?.layoutManager = LinearLayoutManager(requireContext())
            this?.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
            this?.adapter = AlbumRecyclerViewAdapter()
            return@with this?.adapter as AlbumRecyclerViewAdapter
        }
    }

    /**
     * Will init view model and set observers for live data and make api call to get data from server
     */
    private fun initViewModel() {
        viewModel.let {
            observe(it.resource, ::updateData)
            it.fetchData()
        }
    }

    /**
     * Will update data in into views
     */
    private fun updateData(resource: Resource<List<Album>>?) {
        if(resource?.status == Status.SUCCESS) {
            resource.data?.let {
                albumRecyclerViewAdapter?.setUpdatedData(albums = it)
            }
        }
        binding?.resource = resource
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment AlbumListFragment.
         */
        @JvmStatic
        fun newInstance() = AlbumListFragment()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
        albumRecyclerViewAdapter = null
    }
}


