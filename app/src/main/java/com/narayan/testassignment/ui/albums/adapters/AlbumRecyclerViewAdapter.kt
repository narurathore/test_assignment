package com.narayan.testassignment.ui.albums.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.narayan.testassignment.databinding.RecyclerViewItemAlbumBinding
import com.narayan.testassignment.data.models.Album
import com.narayan.testassignment.ui.albums.viewholders.AlbumRecyclerViewHolder

/**
 * Adapter for album list
 */
class AlbumRecyclerViewAdapter: RecyclerView.Adapter<AlbumRecyclerViewHolder>() {

    private var albums: ArrayList<Album> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumRecyclerViewHolder {
        return AlbumRecyclerViewHolder(RecyclerViewItemAlbumBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: AlbumRecyclerViewHolder, position: Int) {
        holder.onBind(albums[position])
    }

    override fun getItemCount(): Int {
        return albums.size
    }

    /**
     * Will update and notify data set
     */
    fun setUpdatedData(albums: List<Album>){
        this.albums.apply {
            clear()
            addAll(albums)
            notifyDataSetChanged()
        }
    }
}