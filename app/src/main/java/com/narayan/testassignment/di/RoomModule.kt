package com.narayan.testassignment.di

import android.content.Context
import androidx.room.Room
import com.narayan.testassignment.data.database.AlbumDatabase
import com.narayan.testassignment.data.database.AlbumsDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

/**
 * Module which provides AlbumDatabase & AlbumsDao
 */
@Module
@InstallIn(SingletonComponent::class)
class RoomModule {

    /**
     * Provides AlbumDatabase
     */
    @Provides
    fun provideDataBase(@ApplicationContext appContext: Context): AlbumDatabase =
        Room.databaseBuilder(appContext, AlbumDatabase::class.java, "album_database")
            .fallbackToDestructiveMigration().build()

    /**
     * Provides AlbumsDao
     */
    @Provides
    fun getAlbumDao(albumDatabase: AlbumDatabase): AlbumsDao = albumDatabase.albumDao()

}