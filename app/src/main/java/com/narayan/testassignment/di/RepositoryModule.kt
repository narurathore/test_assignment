package com.narayan.testassignment.di

import com.narayan.testassignment.data.api.AlbumsServiceInterface
import com.narayan.testassignment.data.database.AlbumsDao
import com.narayan.testassignment.data.repository.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

/**
 * Module which provides Repository
 */
@Module
@InstallIn(ActivityRetainedComponent::class)
object RepositoryModule {

    @Provides
    fun getRepositoryInstance(retroService: AlbumsServiceInterface, albumsDao: AlbumsDao): Repository {
        return Repository(retroService, albumsDao)
    }
}