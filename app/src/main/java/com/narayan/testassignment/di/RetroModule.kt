package com.narayan.testassignment.di

import com.narayan.testassignment.data.api.AlbumsServiceInterface
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Module which provide AlbumsServiceInterface & Retrofit
 */
@Module
@InstallIn(SingletonComponent::class)
object RetroModule {

    /**
     * Provides AlbumsServiceInterface
     */
    @Provides
    fun getRetroServiceInterface(retrofit: Retrofit): AlbumsServiceInterface {
        return retrofit.create(AlbumsServiceInterface::class.java)
    }

    /**
     * Provides Retrofit
     */
    @Provides
    fun getRetroFitInstance(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    const val baseUrl = "https://jsonplaceholder.typicode.com/"
}