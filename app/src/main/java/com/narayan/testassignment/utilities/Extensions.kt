package com.narayan.testassignment.utilities

import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.narayan.testassignment.data.models.Album

/**
 * Will make view visible
 */
fun View.visible() {
    visibility = View.VISIBLE
}

/**
 * Will make view gone
 */
fun View.gone() {
    visibility = View.GONE
}

/**
 * To observe live data
 */
fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T?) -> Unit) {
    liveData.observe(this, Observer(body))
}

/**
 * Will sort the albums in ascending order
 * @return List<Album> sorted list of album
 */
fun List<Album>.sortByTitle() : List<Album> {
    return this.sortedWith(compareBy { it.title })
}