package com.narayan.testassignment.utilities

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}